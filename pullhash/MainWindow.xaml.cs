﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace PullHash
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    public partial class MainWindow
    {
        private readonly MD5CryptoServiceProvider MD5Algorithm = new MD5CryptoServiceProvider();
        private readonly SHA1CryptoServiceProvider SHA1Algorithm = new SHA1CryptoServiceProvider();
        private readonly SHA256CryptoServiceProvider SHA256Algorithm = new SHA256CryptoServiceProvider();
        private static readonly string[] SizeSuffixes ={ "bytes", "KB", "MB", "GB" };
        private const int _BufferSize = 128 * 1024;
        private string FilePath;
        private long FileSize;

        private readonly History historyWindow = new History();
        private readonly ObservableCollection<HistoryField> history = new ObservableCollection<HistoryField>();


        public MainWindow()
        {
            InitializeComponent();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }

        private async void Calculate(string path, long filesize, string filename)
        {
            this.MD5Entry.Text = this.SHA1Entry.Text = this.SHA256Entry.Text = "Calculating...";

            using (var stream = new FileStream(path, FileMode.Open,
                       FileAccess.Read, FileShare.Read, _BufferSize, true))
            {
                this.MD5Algorithm.Initialize();
                this.SHA1Algorithm.Initialize();
                this.SHA256Algorithm.Initialize();

                var buffer = new byte[_BufferSize];
                int bytesRead;
                do
                {
                    bytesRead = await stream.ReadAsync(buffer, 0, _BufferSize);
                    if (bytesRead <= 0) continue;
                    await Task.Run(() => { this.MD5Algorithm.TransformBlock(buffer, 0, bytesRead, null, 0); });
                    await Task.Run(() => { this.SHA1Algorithm.TransformBlock(buffer, 0, bytesRead, null, 0); });
                    await Task.Run(() => { this.SHA256Algorithm.TransformBlock(buffer, 0, bytesRead, null, 0); });
                } while (bytesRead > 0);


                this.MD5Algorithm.TransformFinalBlock(buffer, 0, 0);
                this.SHA1Algorithm.TransformFinalBlock(buffer, 0, 0);
                this.SHA256Algorithm.TransformFinalBlock(buffer, 0, 0);

                this.MD5Entry.Text = BitConverter.ToString(this.MD5Algorithm.Hash).Replace("-", "").ToLowerInvariant();
                this.SHA1Entry.Text = BitConverter.ToString(this.SHA1Algorithm.Hash).Replace("-", "").ToLowerInvariant();
                this.SHA256Entry.Text = BitConverter.ToString(this.SHA256Algorithm.Hash).Replace("-", "").ToLowerInvariant();
            }

            var newField = new HistoryField
            {
                FileName = filename,
                FileSize = SizeSuffix(filesize)
            };
            
            newField.HashResults.Add(new HashResults()
            {
                MD5Hash = this.MD5Entry.Text,
                SHA1Hash = this.SHA1Entry.Text,
                SHA256Hash = this.SHA256Entry.Text
            });
            this.history.Add(newField);
            Trace.WriteLine(this.history.Count);
            this.historyWindow.historyTree.ItemsSource = this.history;
        }
        
        private static string SizeSuffix(long value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0)
            {
                throw new ArgumentOutOfRangeException("decimalPlaces");
            }

            if (value < 0)
            {
                return "-" + SizeSuffix(-value);
            }

            if (value == 0)
            {
                return string.Format("{0:n" + decimalPlaces + "} bytes", 0);
            }

            var mag = (int)Math.Log(value, 1024);

            var adjustedSize = (decimal)value / (1L << (mag * 10));

            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        private bool HistoryFieldExists(
            IEnumerable<HistoryField> history,
            string path)
        {
            return history.Any(item => item.FileName == Path.GetFileName(path));
        }

        private bool IsDirectory(
            IEnumerable<string> files)
        {
            return files.Any(str => File.GetAttributes(str) == FileAttributes.Directory);
        }

        private void Window_Drop(
            object sender,
            DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var droppedFiles = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                if (droppedFiles != null && !(droppedFiles.Length > 1 && !this.IsDirectory(droppedFiles)))
                {
                    this.FilePath = droppedFiles[0];
                    this.FileSize = new FileInfo(this.FilePath).Length;

                    if (!this.HistoryFieldExists(this.history, this.FilePath)) this.Calculate(this.FilePath, this.FileSize, Path.GetFileName(FilePath));
                }
                else
                {
                    MessageBox.Show("The hash is calculated only for one file!",
                        "Ops..", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void CompareWithEntry_TextChanged(
            object sender,
            TextChangedEventArgs e)
        {
            if (this.CompareWithEntry.Text.Length > 0)
            {
                this.MD5Entry.Background = this.MD5Entry.Text == this.CompareWithEntry.Text ? Brushes.LightGreen : Brushes.White;
                this.SHA1Entry.Background = this.SHA1Entry.Text == this.CompareWithEntry.Text ? Brushes.LightGreen : Brushes.White;
                this.SHA256Entry.Background = this.SHA256Entry.Text == this.CompareWithEntry.Text ? Brushes.LightGreen : Brushes.White;
            }
        }

        private void Hyperlink_RequestNavigate(
            object sender,
            RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void Button_Click(
            object sender,
            RoutedEventArgs e)
        {
            if (!this.historyWindow.IsVisible)
                this.historyWindow.Show();
            else
                this.historyWindow.Hide();
        }

        private void Window_Closing(
            object sender,
            CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
